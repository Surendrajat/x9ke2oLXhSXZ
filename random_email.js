const fakerator = require("fakerator")("de-DE");

module.exports = {
    get: function () {
        fakerator.seed(Date.now())
        var email = fakerator.internet.email();
        var email_parts = String(email).split("@");
        email = email_parts[0] + fakerator.internet.color() + "@" + email_parts[1];
        return email;
    }
};