const puppeteer = require('puppeteer');
const fake_email = require('./random_email');

process.setMaxListeners(Infinity);

function test_vote() {

    (async () => {
        const browser = await puppeteer.launch({
            args: [
                '--disable-setuid-sandbox',
                '--no-sandbox',
                '--ignore-certificate-errors',
                '--disable-gpu',
            ],
            ignoreHTTPSErrors: true,
            headless: true
        });
        const page = await browser.newPage();
        const forum_url = "https://microsoftteams.uservoice.com/forums/555103-public/suggestions/40324324-keep-the-linux-client-up-to-date-with-the-latest-f";

        try {

            await page.goto(forum_url);
            // let it load 
            await page.waitForXPath("//form[@class='uvIdeaVoteForm']");
            // await page.screenshot({
            //     path: "step1.png"
            // })
            // click the Vote button
            await page.click('button.uvIdeaVoteFormTriggerState-no_votes.uvStyle-button')
            // let the form open
            await page.waitForXPath("//form[@class='uvIdeaVoteForm uvSignin-prestine']");
            // await page.screenshot({
            //     path: "step2.png"
            // })
            // click on the email div
            await page.click('div.uvField.uvField-email.uvField-first')
            // await page.screenshot({
            //     path: "step3.png"
            // })
            // fill email id
            await page.type('div.uvField.uvField-email.uvField-first > label > input', fake_email.get())
            // await page.screenshot({
            //     path: "step4.png"
            // })
            // wait for validation
            await page.waitForXPath("//form[@class='uvIdeaVoteForm uvSignin-display_name']");
            // await page.screenshot({
            //     path: "step5.png"
            // })
            // check the ToS checkbox
            await page.click('div.uvVoter.uvVoter-logged_out.uvVoterMode-voteable.uvVoterStatus-open.uvVoteLimit-unlimited > form > div > fieldset > div > fieldset > div.uvField.uvField-consent.tos-consent > label')
            // await page.screenshot({
            //     path: "step6.png"
            // })
            // click the Vote finally
            await page.click('div.uvIdeaVoteButtons > div.submit_region.auth-only > button')
            // wait for the Voted button
            await page.waitFor('div.uvVoter.uvVoterStatus-open.uvVoteLimit-unlimited.uvVoterMode-voted')
            // await page.screenshot({
            //     path: "step7.png"
            // })
        } catch (e) {
            console.log(e);
        } finally {
            await browser.close();
        }
    })();
}

module.exports = {
    run: function () {
        test_vote();
    }
};
